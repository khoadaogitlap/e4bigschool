﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(E4BigSchool1.Startup))]
namespace E4BigSchool1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
